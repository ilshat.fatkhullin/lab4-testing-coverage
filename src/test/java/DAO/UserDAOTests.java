package DAO;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDAOTests {
    private JdbcTemplate mockJdbcTemplate;
    private UserDAO userDAO;
    private final String mockNickname = "nickname";
    private final String mockEmail = "email";
    private final String mockFullName = "full_name";
    private final String mockAbout = "about";


    @BeforeEach
    void setUp() {
        mockJdbcTemplate = Mockito.mock(JdbcTemplate.class);
        userDAO = new UserDAO(mockJdbcTemplate);
    }

    @Test
    void changeSuccess() {
        UserDAO.Change(new User(mockNickname, null, null, null));
        Mockito.verifyNoInteractions(mockJdbcTemplate);
    }

    @Test
    void changeSuccess2() {
        UserDAO.Change(new User(mockNickname, mockEmail, null, null));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockEmail),
                Mockito.eq(mockNickname));
    }

    @Test
    void changeSuccess3() {
        UserDAO.Change(new User(mockNickname, null, mockFullName, null));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockFullName),
                Mockito.eq(mockNickname));
    }

    @Test
    void changeSuccess4() {
        UserDAO.Change(new User(mockNickname, null, null, mockAbout));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockAbout),
                Mockito.eq(mockNickname));
    }

    @Test
    void changeSuccess5() {
        UserDAO.Change(new User(mockNickname, mockEmail, mockFullName, null));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockEmail),
                Mockito.eq(mockFullName),
                Mockito.eq(mockNickname));
    }

    @Test
    void changeSuccess6() {
        UserDAO.Change(new User(mockNickname, mockEmail, null, mockAbout));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockEmail),
                Mockito.eq(mockAbout),
                Mockito.eq(mockNickname));
    }

    @Test
    void changeSuccess7() {
        UserDAO.Change(new User(mockNickname, null, mockFullName, mockAbout));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockFullName),
                Mockito.eq(mockAbout),
                Mockito.eq(mockNickname));
    }

    @Test
    void changeSuccess8() {
        UserDAO.Change(new User(mockNickname, mockEmail, mockFullName, mockAbout));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(mockEmail),
                Mockito.eq(mockFullName),
                Mockito.eq(mockAbout),
                Mockito.eq(mockNickname)
        );
    }

}
